<?php

$codsemestre = '2020';
$semestre_ini = '2019-11-01 00:00:00';
$semestre_fim = '2021-02-01 00:00:00';

$tables = array(
    'alunoturma_gr'=> new UspTable('alunoturma_gr', 
                                     array('id'=>'id',
                                           'coddis'=>'s',
                                           'codtur'=>'s',
                                           'codpes'=>'n',
                                           'verdis'=>'n',
                                           'stamtr' => 's'),
                                     'select coddis,codtur,codpes,verdis,stamtr from CEPAVIEW_HISTESCOLARGR'.
                                     " where codtur like '$codsemestre%' AND stamtr in ('I','P','M')"
                                     ),
    
    'disciplinas_gr'=> new UspTable('disciplinas_gr', 
                                      array('id'=>'id',
                                            'coddis'=>'s',
                                            'verdis'=>'n',
                                            'nomdis'=>'u',
                                            'creaul'=>'n',
                                            'cretrb'=>'n',
                                            'dtaatvdis'=>'d',
                                            'dtadtvdis'=>'d',
                                            'durdis'=>'n',
                                            'objdis'=>'u',
                                            'numvagdis'=>'n',
                                            'numvagdiscpl'=>'n',
                                            'pgmdis'=>'u',
                                            'pgmrsudis'=>'u',
                                            'tipdis'=>'s',
                                            'cgahoreto'=>'n',
                                            'staarecln'=>'s'),
                                      "select * from CEPAVIEW_DISCIPLINAGR"
                                      ),

    'turmas_gr'=> new UspTable('turmas_gr',
                                 array('id'=>'id',
                                       'coddis'=>'s',
                                       'verdis'=>'n',
                                       'codtur'=>'s',
                                       'dtainitur'=>'d',
                                       'dtafimtur' => 'd',
                                       'codpes'=>'n'),
                                 'select distinct T.coddis, T.verdis, T.codtur, T.dtainitur, T.dtafimtur, M.codpes from CEPAVIEW_TURMAGR T, CEPAVIEW_MINISTRANTE M ' .
                                 "where T.coddis = M.coddis AND T.codtur = M.codtur AND T.verdis = M.verdis AND T.dtainitur > '$semestre_ini' AND T.statur = 'A' AND T.codtur like '$codsemestre%'"
                                 ),

    'turmas_ad_gr'=> new UspTable('turmas_ad_gr',
                                 array('id'=>'id',
                                       'coddis'=>'s',
                                       'verdis'=>'n',
                                       'codtur'=>'s',
                                       'dtainitur'=>'d',
                                       'dtafimtur' => 'd',
                                       'codpes'=>'n'),
                                 'select distinct T.coddis, T.verdis, T.codtur, T.dtainitur, T.dtafimtur, A.codpes from CEPAVIEW_TURMAGR T, CEPAVIEW_ATIVIDADEDOCENTE A ' .
                                 "where T.coddis = A.coddis AND T.codtur = A.codtur AND T.verdis = A.verdis AND T.dtainitur > '$semestre_ini' AND T.statur = 'A' AND T.codtur like '$codsemestre%'"
                                 ),

    'turmas_resp_gr' => new UspTable('turmas_resp_gr',
                                    array('id'=>'id',
                                       'coddis'=>'s',
                                       'verdis'=>'n',
                                       'codtur'=>'s',
                                       'dtainitur'=>'d',
                                       'dtafimtur' => 'd',
                                       'codpes'=>'n'),
                                    'select distinct T.coddis, T.verdis, T.codtur, T.dtainitur, T.dtafimtur, R.codpes from CEPAVIEW_TURMAGR T, CEPAVIEW_DISCIPGRRESP R' .
                                 " where T.coddis = R.coddis AND T.dtainitur > '$semestre_ini' AND T.statur = 'A' AND T.codtur like '$codsemestre%' and R.dtafimrsp is null"
                                 ),

    'alunoturma_pos'=>new UspTable('alunoturma_pos', 
                                     array('id'=>'id',
                                           'sgldis'=>'s',
                                           'numseqdis'=>'n',
                                           'numofe'=>'n',
                                           'codpes'=>'n',
                                           'stamtrpgmofe' => 's'),
                                     'select distinct sgldis, numseqdis, numofe, codpes, stamtrpgmofe from CEPAVIEW_R41PGMMATTUR '.
                                     "where dtarefpgmofe > '$semestre_ini' AND dtarefpgmofe < '$semestre_fim' AND stamtrpgmofe in ('P', 'A', 'D')"
                                     ),

    'disciplinas_pos'=> new UspTable('disciplinas_pos', 
                                       array('id'=>'id',
                                             'sgldis'=>'s',
                                             'numseqdis'=>'n',
                                             'dtaprpdis'=>'d',
                                             'nomdis'=>'u',
                                             'tipcurdis'=>'s',
                                             'durdis'=>'n',
                                             'cgahorteodis'=>'n',
                                             'cgahoresddis'=>'n',
                                             'cgahordis'=>'n',
                                             'numcretotdis'=>'n',
                                             'dtaatvdis'=>'d',
                                             'dtadtvdis'=>'d',
                                             'objdis'=>'u',
                                             'jusdis'=>'u',
                                             'tipavldis'=>'u',
                                             'ctudis'=>'u',
                                             'ctubbgdis'=>'u',
                                             'obsdis'=>'u',
                                             'codare'=>'n',
                                             'dtaaprcog'=>'d'),
                                       'select * from CEPAVIEW_DISCIPLINA'
                                       ),
    
    'turmas_pos'=> new UspTable('turmas_pos',
                                  array('id'=>'id',
                                        'sgldis'=>'s',
                                        'numseqdis'=>'n',
                                        'numofe'=>'n',
                                        'dtainiofe'=>'d',
                                        'dtafimofe'=>'d',
                                        'codpes'=>'n'),
                                  'select O.sgldis, O.numseqdis, O.numofe, O.dtainiofe, O.dtafimofe, M.codpes from CEPAVIEW_OFERECIMENTO O, CEPAVIEW_R32TURMINDOC M ' .
                                  "where O.sgldis = M.sgldis AND O.numseqdis = M.numseqdis AND O.numofe = M.numofe AND O.dtainiofe > '$semestre_ini' AND O.dtacantur is NULL"
                                 ),
    
    'unidades'=> new UspTable('unidades', 
                              array('id'=>'id',
                                    'codfusclgund'=>'n',
                                    'codoriclgund'=>'n',
                                    'nomclgund'=>'u',
                                    'sglfusclgund'=>'u',
                                    'clsgrpanr'=>'n',
                                    'grpsglanr'=>'u',
                                    'nomloc'=>'u',
                                    'urlsie'=>'s'),
                              'select codfusclgund,codoriclgund,nomclgund,sglfusclgund,clsgrpanr,grpsglanr,nomloc,urlsie from CEPAVIEW_FUSAOCOLEGIADOUNIDADE ' .
                              'left join CEPAVIEW_URLUNIDADE on CEPAVIEW_FUSAOCOLEGIADOUNIDADE.codoriclgund=CEPAVIEW_URLUNIDADE.codund '.
                              'where stasieref=\'S\' OR urlsie is NULL'
                              ),
    
    'pessoa'=> new UspTable('pessoa', 
			    array('id'=>'id',
                                  'codpes'=>'n',
                                  'nompes'=>'u',
                                  'numcpf'=>'h',
                                  'cpf' => 'n',
                                  'sexpes'=>'s',
                                  'dtanas'=>'d',
                                  'tipdocidf' => 's',
                                  'numdocidf' => 'h'), 
			    "select codpes,nompes,numcpf,numcpf as cpf,sexpes,dtanas, tipdocidf, numdocidf from CEPAVIEW_PESSOA "

			  //" where dtaultalt = '__LAST_SYNC__' or dtaultalt > '__LAST_SYNC__' or (dtaultalt is NULL and (dtacadpes = '__LAST_SYNC__' or dtacadpes > '__LAST_SYNC__'))"			    
                            ),

    'vinculopessoausp'=> new UspTable('vinculopessoausp',
                                      array('id'=>'id',
                                            'codpes'=>'n',
                                            'tipvin'=>'s',
                                            'numseqpes'=>'n',
                                            'dtainivin'=>'d',
                                            'dtafimvin'=>'d',
                                            'sitatl'=>'s',
                                            'tipcon'=>'s',
                                            'dtainisitatl'=>'d',
                                            'codfusclgund'=>'n'),
                                      "select codpes, tipvin, numseqpes, dtainivin, dtafimvin, sitatl, tipcon, dtainisitatl, codfusclgund from CEPAVIEW_VINCULOPESSOAUSP "
				      //     " where dtaultalt = '__LAST_SYNC__' or dtaultalt > '__LAST_SYNC__' or (dtaultalt is NULL and (dtainivin = '__LAST_SYNC__' or dtainivin > '__LAST_SYNC__'))"
                                      ),

    'emailpessoa'=> new UspTable('emailpessoa',
				 array('id'=>'id',
                                       'codpes'=>'n',
                                       'codema'=>'u',
                                       'numseqema'=>'n',
                                       'stamtr'=>'s',
                                       'staatnsen'=>'s'),
				 "select codpes, codema, numseqema, stamtr, staatnsen from CEPAVIEW_EMAILPESSOA "
				 //"where ((dtaultalt = '__LAST_SYNC__' or dtaultalt > '__LAST_SYNC__') or (dtaultalt is NULL and (dtacad = '__LAST_SYNC__' or dtacad > '__LAST_SYNC__')))",
                                 ), 
    
    'prefixodiscip'=> new UspTable('prefixodiscip', 
				   array('id'=>'id',
                                         'sglund'=>'u',
                                         'pfxdisval'=>'s',
                                         'dscpfxdis'=>'u',
                                         'nomund'=>'u'),
                                   // 'select distinct F.sglfusclgund, P.pfxdisval, P.dscpfxdis, F.nomclgund from CEPAVIEW_PREFIXODISCIP P, CEPAVIEW_FUSAOCOLEGIADOUNIDADE F where P.codclg = F.codfusclgund '
                                          'select distinct u.sglund, p.pfxdisval, p.dscpfxdis, u.nomund from CEPAVIEW_PREFIXODISCIP p left join CEPAVIEW_COLEGIADO c on p.codclg = c.codclg and p.sglclg = c.sglclg left join CEPAVIEW_UNIDADE u on c.codundrsp = u.codund where p.dtadtvpfx = null'
    ),
    
    'sitalunoativogr'=> new UspTable('sitalunoativogr',
                  array('id'=>'id',
                  'codpes'=>'n',
                  'codpgm'=>'n',
                  'anosem'=>'n',
                  'codclg'=>'n',
                  'sglclg'=>'s',
                  'staalu'=>'s',
                  'codcur'=>'n',
                  'codhab'=>'n',
                  'dtager'=>'d',
                  'vinpgm'=>'s',
                  'perhab'=>'s',
                  'stalcnand'=>'s'),
    "select codpes, codpgm, anosem, codclg, sglclg, staalu, codcur, codhab, dtager, vinpgm, perhab, stalcnand from CEPAVIEW_SITALUNOATIVOGR where anosem > 20120"
    ),

    'unidade' => new UspTable('unidade',
    array('id'=>'id',
    'codund'=>'n',
    'tipund'=>'u',
    'codcam'=>'n',
    'sglund'=>'s',
    'nomund'=>'u'),
    "select codund, tipund, codcam, sglund, nomund from CEPAVIEW_UNIDADE"
    ),

    'unidcoleg' => new UspTable('unidcoleg',
    array('id'=>'id',
    'codclg'=>'n',
    'sglclg'=>'s',
    'codund'=>'n'),
    "select codclg, sglclg, codund from CEPAVIEW_UNIDCOLEG"
    ),

    'cursogr' => new UspTable('cursogr',
    array('id'=>'id',
    'codcur'=>'n',
    'codclg'=>'n',
    'sglclg'=>'s',
    'nomcur'=>'u'),
    "select codcur, codclg, sglclg, nomcur from CEPAVIEW_CURSOGR"
    ),

    'habilitacaogr' => new UspTable('habilitacaogr',
    array('id'=>'id',
    'codcur'=>'n',
    'codhab'=>'n',
    'nomhab'=>'u',
    'tiphab'=>'s',
    'perhab'=>'u'),
    "select codcur, codhab, nomhab, tiphab, perhab from CEPAVIEW_HABILITACAOGR"
    ),
    
);
?>
