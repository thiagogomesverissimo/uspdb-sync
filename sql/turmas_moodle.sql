CREATE TABLE turmas_moodle (
    id int unsigned NOT NULL auto_increment,
    codmoodle varchar(22) NOT NULL,
    codpes int unsigned NOT NULL,
    dataini date,
    datafim date,
    tipo CHAR(3),
    PRIMARY KEY(id),
    KEY `idx_codpes` (`codpes`),
    KEY `idx_codmoodle` (`codmoodle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
