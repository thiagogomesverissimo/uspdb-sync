create table sitalunoativogr (
       id int unsigned NOT NULL auto_increment,
       codpes int not null,
       codpgm tinyint not null,
       anosem int not null,
       codclg smallint not null,
       sglclg char(12) not null,
       staalu char(1) not null,
       codcur int null,
       codhab smallint null,
       dtager date not null,
       vinpgm char(2) not null,
       perhab char(10) null,
       stalcnand char(1) null,
       PRIMARY KEY(id),
       KEY `idx_anosem` (`anosem`),
       KEY `idx_codclg` (`codclg`),
       KEY `idx_codcur` (`codcur`),
       KEY `idx_codhab` (`codhab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
