create table cursogr (
       id int unsigned NOT NULL auto_increment,
       codcur int not null,
       codclg smallint not null,
       sglclg char(12) not null,
       nomcur varchar(100) null,
       PRIMARY KEY(id),
       KEY `idx_sglclg` (`sglclg`),
       KEY `idx_codclg` (`codclg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

