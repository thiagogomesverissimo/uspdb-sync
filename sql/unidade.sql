create table unidade (
    id int unsigned NOT NULL auto_increment,
    codund smallint not null,
    tipund varchar(35) not null,
    codcam smallint not null,
    sglund char(7) not null,
    nomund varchar(80) not null,
    PRIMARY KEY(id),
    KEY `idx_codund` (`codund`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
