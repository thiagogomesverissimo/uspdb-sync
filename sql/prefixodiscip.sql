
CREATE TABLE prefixodiscip (
    id int unsigned NOT NULL auto_increment,
    sglfusclgund varchar(40) NOT NULL,
    pfxdisval char(3) NOT NULL,
-- prefixo da disciplina
    dscpfxdis varchar(100),
-- descrição do órgão
    nomclgund varchar(180),
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

