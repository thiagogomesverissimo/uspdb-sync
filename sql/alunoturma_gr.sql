CREATE TABLE alunoturma_gr (
    id int unsigned NOT NULL auto_increment,
    coddis char(7) NOT NULL,
    codtur char(10) NOT NULL,
    codpes int unsigned NOT NULL,
    verdis tinyint unsigned NOT NULL,
    stamtr char(1) NOT NULL,
    PRIMARY KEY(id),
    KEY `idx_codpes` (`codpes`),
    KEY `idx_coddis` (`coddis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
