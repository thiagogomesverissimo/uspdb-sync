-- -----------------------------------------------------
-- Table `usp`.`vinculopessoausp`
-- -----------------------------------------------------
CREATE  TABLE `vinculopessoausp` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `codpes` INT UNSIGNED NOT NULL COMMENT 'Número USP' ,
  `tipvin` VARCHAR(25) NULL COMMENT 'tipo de vínculo com a usp: alunogr, alunopos, docente, servidor, estagiário etc. Uma pessoa pode ter vários vínculos' ,
  `numseqpes` INT NULL ,
  `dtainivin` DATE NULL ,
  `dtafimvin` DATE NULL ,
  `sitatl` CHAR(1) NULL COMMENT 'situação do vínculo (Ativo, aPosentado, Desligado)' ,
  `tipcon` VARCHAR(25) NULL,
  `dtainisitatl` DATE NULL ,
  `codfusclgund` SMALLINT NULL COMMENT 'código da unidade' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
