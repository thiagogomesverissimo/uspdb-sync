<?php

class UspTable {

    public function __construct($name, $fields, $sql, $delete=true, $replace=false){
        $this->name = $name;
        $this->fields = $fields;
        $this->sql = $sql;
        $this->delete = $delete;
        $this->replace = $replace;
    }

    /**
     * (re-)creates the table
     * if $dicce is given, also re-populate it
     **/
    public function recreate($dblocal) {
        global $verbose, $CFG;

        $query = "drop table if exists {$this->name}";
        if($verbose) {
            print "dropping table {$this->name}";
        }
        
        if( ! $dblocal->query($query)){
            print("Erro dropping {$this->name}. \n".$dblocal->error."\n");
            return false;
        }
        $fn = "sql/{$this->name}.sql";
        $query = file_get_contents($fn);
        if($verbose) {
            print("creating {$this->name} from {$fn}");
            print($query);
        }
        
        if( ! $dblocal->query($query)){
            print("Erro creating {$this->name}. \n".$dblocal->error."\n");
            return false;
        }
        return true;
    }
    
    /**
     * processa e insere dados de uma tabela
     *
     * recebe link resource do db local e do dicce
     *
     * retorna true/false em caso de sucesso/falha
     */
    public function process($dblocal, $dicce){
        global $verbose, $CFG;

        // se tabela apenas faz update, checar data da última sync
        if($this->delete == false){

            $last_sync = $this->get_last_sync($dblocal);
            if($verbose)
                print "Última atualização: $last_sync\n";

            $sql = str_replace("'__LAST_SYNC__'", $last_sync, $this->sql);

        } else
            $sql = $this->sql;

        if($verbose)
            print "$sql\n";

        $res = $dicce->query($sql);

        if($verbose)
            print "Query executado no DICCE.\n";

        // apaga dados antigos
        if($this->delete){
            if($dblocal->query("delete from {$this->name}")) {
                if($verbose)
                    print "Dados antigos da tabela {$this->name} apagados.\n";
            } else {
                print("Não apagou {$this->name}. Problema com conexão ao base local\n");
                exit(1);
            }
        }
        
        // processando dados

        $count = 0;
        $records = array();
        $maxrecords = $CFG->maxrecords;
        
        try {
            foreach ($res as $row){
                $records[] = $this->process_data($row, $dblocal);
                // não inserir dados demais de uma vez - evitar problemas de memória
                $count++;
                if ($count >= $maxrecords){
                    $insert = $this->replace ? 'replace' : 'insert';
                    $query = "$insert into {$this->name} values " . implode(',',$records);
                    if($verbose) {
                        print "{$insert}ing next $maxrecords\n";
                    }
                    
                    if( ! $dblocal->query($query)){
                        print("Erro na inserção de dados na tabela {$this->name}. \n".$dblocal->error."\n");
                        print("record: {$records[0]} \n");
                        //continue;
                        return false;
                    }
                    
                    $count = 0;
                    $records = array();
                    sleep($CFG->sleeptime);
                }
            }
        } catch (PDOException $e) {
            print "PDO error!: " . $e->getMessage();
            return false;
        }
        
        if($count > 0){
            $insert = $this->replace ? 'replace' : 'insert';
            $query = "$insert into {$this->name} values " . implode(',',$records);
            if($verbose) {
                print "{$insert}ing last $count rows";
            }
                
            if( ! $dblocal->query($query)){
                print("Erro na inserção de dados na tabela {$this->name}. \n".$dblocal->error."\n");
                return false;
            }
        }
        
        // insere data de atualização no bd
        if( ! $this->set_last_sync($dblocal) ) {
            print("Data de atualização não foi inserida no banco de dados!\n");
        } elseif($verbose) { 
            print "Data de atualização inserida";
        }
                
        return true;
        
    }


    public function process_data($row, $dblocal){

        $values = array();

        foreach($this->fields as $fieldname=>$fieldspec){
            if(array_key_exists($fieldname, $row)){
                switch ($fieldspec){
                case 'd':
                case 'date':
                case 'data':
                    $newvalue = $this->smalldate_to_datetime($row[$fieldname]);
                    break;
                case 'n':
                case 'number':
                    if(is_numeric($row[$fieldname])) {
                        $newvalue = $row[$fieldname];
                    } else {
                        $newvalue = 'null';
                    }
                    break;
                case 's':
                case 'string':
                    $newvalue = '"'.$dblocal->real_escape_string($row[$fieldname]).'"';
                    break;
                case 'u':
                case 'utf8':
                case 'text':
                    $newvalue = '"'.iconv("ISO-8859-1", "UTF-8",$dblocal->real_escape_string($row[$fieldname])).'"';
                    break;
                case 'h':
                case 'hash':
                    global $CFG; // ugly, ugly
                    if(!empty($row[$fieldname])){
    					$newvalue = "'".md5($CFG->salt . $row[$fieldname])."'";
    				} else {
    					$newvalue = "NULL";
                    }
                    break;
                default:
                    $newvalue = null;
                    break;
                }
                if(isset($newvalue))
                    $values[] = $newvalue;
            
            } elseif($fieldspec == 'id')
                $values[] = "null";
        }
        return '(' . implode(',', $values) . ')';
    }

    /**
     * checa data da última atualização de uma tabela
     *
     * recebe db resource
     *
     * retorna data da última atualização (entre aspas), se houver, ou 'NULL' caso contrário
     */
    public function get_last_sync($dblocal){

	$sql = "SELECT * FROM sync_times WHERE name = '{$this->name}' LIMIT 1";
	if(!$result = $dblocal->query($sql )){
	    print("Erro ao recuperar data da última sincronização: " . $dblocal->error . "\n");
	    return 'NULL';
        }
        else{ 
	    if($result->num_rows > 0){
 		if ($r = $result->fetch_assoc()){
	   		return "'{$r['sync']}'";
	        }
	    }
	    else return 'NULL';
        }
    }

    /**
     * insere data da última atualização de uma tabela
     *
     * recebe db resource
     *
     * retorna true em caso de sucesso, ou false caso contrário
     */
    public function set_last_sync($dblocal){
        global $verbose;

        $time = date('Y-m-d H:i:s');
        $sql = "REPLACE INTO sync_times VALUE('{$this->name}', '$time')";

        $res = $dblocal->query($sql);
        if(!$res){
            if($verbose){
                $err
                    = "QUERY FAIL: "
                    . $sql
				. ' ERRNO: '
                    . $mysqli->errno
                    . ' ERROR: '
                    . $mysqli->error
                    ;
                trigger_error($err, E_USER_ERROR);
            }
		return false;
        }
        return true;
    }
    


    // converts sybase smalldate data type to mysql datetime format
    private function smalldate_to_datetime($date){
        if(empty($date))
            return "NULL";
        else {
            // try to fix $date, removing microseconds
            //$date = preg_replace('|(\w \d{1,2} \d{4} \d{2}:\d{2}:\d{2}):\d{3}(\w)|i', '${1}${2}', $date);
            $date = preg_replace('/:000(AM|PM)$/i', "$1", $date); 
            return '"' . date("Y-m-d H:i:s", strtotime($date)) . '"';
        }
    }
}

?>
